jQuery(function ($) {
    $('*[data-wa]').click(function() {
        var target = $(this).data('wa');
        var message = $(this).data('message');

        if (typeof message !== 'undefined') {
            window.open("https://api.whatsapp.com/send?phone="+target+"&text="+message);
        }
        else {
            window.open("https://api.whatsapp.com/send?phone="+target);
        } 
    });
});
