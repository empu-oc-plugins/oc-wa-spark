<?php namespace Empu\WaSpark;

use Backend;
use Empu\WaSpark\Components\WaButton;
use System\Classes\PluginBase;

/**
 * WaSpark Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'WA Spark',
            'description' => 'Whatsapp chat widget for OctoberCMS',
            'author'      => 'Wuri Nugrahadi',
            'icon'        => 'icon-whatsapp'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            WaButton::class => 'WaButton',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'empu.waspark.access_settings' => [
                'tab' => 'WA Spark',
                'label' => 'Change whatsapp widget settings'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'waspark' => [
                'label'       => 'WaSpark',
                'url'         => Backend::url('empu/waspark/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['empu.waspark.*'],
                'order'       => 500,
            ],
        ];
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'WA Spark Settings',
                'description' => 'Manage whatsapp widget settings.',
                'category'    => 'Marketing',
                'icon'        => 'icon-whatsapp',
                'class'       => 'Empu\WaSpark\Models\Settings',
                'order'       => 500,
                'keywords'    => 'wa whatsapp spark chat widget',
                'permissions' => ['empu.waspark.access_settings']
            ]
        ];
    }
}
