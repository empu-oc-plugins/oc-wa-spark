<?php namespace Empu\WaSpark\Components;

use Cms\Classes\ComponentBase;
use Empu\WaSpark\Models\Settings;

class WaButton extends ComponentBase
{
    /**
     * Button text
     *
     * @var string
     */
    public $buttonText;

    /**
     * WA recepient number
     *
     * @var string
     */
    public $waNumber;

    /**
     * Message to send
     *
     * @var string
     */
    public $message;

    /**
     * Custom style apply to button
     *
     * @var string
     */
    public $style;

    public function componentDetails()
    {
        return [
            'name'        => 'Whatsapp Button',
            'description' => 'WA simple chat button'
        ];
    }

    public function defineProperties()
    {
        return [
            'buttonText' => [
                'title' => 'Button text',
                'description' => 'Text displayed on whatsapp chat button',
                'default' => Settings::get('title'),
                'type' => 'string',
            ],
            'waNumber' => [
                'title' => 'Whatsapp number',
                'description' => 'CS number',
                'default' => Settings::get('wa_number'),
                'type' => 'string',
            ],
            'message' => [
                'title' => 'Default message',
                'description' => 'Intro message send to you',
                'type' => 'string',
            ],
            'floating' => [
                'title' => 'Floating button',
                'description' => 'Display button floating over page',
                'type' => 'checkbox',
            ],
            'style' => [
                'title' => 'Custom style',
            ]
        ];
    }

    public function onRun()
    {
        $this->buttonText = $this->property('buttonText', Settings::get('title'));
        $this->waNumber = $this->property('waNumber', Settings::get('wa_number'));
        $this->message = $this->property('message');

        if ($this->property('floating')) {
            $this->style = 'position: fixed; z-index: 99999;';
            $this->style .= $this->property('style', 'bottom: 20px; right: 20px;');
        }
        else {
            $this->style = $this->property('style');
        }

        
        $this->addCss('assets/css/wa-button.css');
        $this->addJs('assets/js/simple.js');
    }
}
